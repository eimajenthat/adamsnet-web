Adamsnet Web Site
=================

My personal web site, based on Symfony 2.1.  See Composer.json for more requirements.


Requirements
------------
- PHP 5.3
- Web server (tested with Apache so far)
- MongoDB (see below)
- [Composer](http://getcomposer.org/download/)
- Node.js (to run LessCSS, better solutions are welcome)


Setup MongoDB
-------------
1. You will need PHP 5.3, a web server.

1. Install [MongoDB](http://www.mongodb.org/display/DOCS/Quickstart) if you don't already have it.

1. Install MongoDB driver extension for PHP.  If you're lucky, it's just:

        sudo pecl install mongo

    If not, [check here](http://php.net/manual/en/mongo.installation.php) (On OSX, [I had to install PEAR](http://stackoverflow.com/a/9556683)).

1. Once install, make sure you add:

        extension=mongo.so

to your php.ini file.


Installation and Setup
----------------------

1. Clone the repo:

        git clone git@bitbucket.org:eimajenthat/adamsnet-web.git

1. Install PHP package dependencies:

        php composer.phar install

1. Run Symfony's builtin check script.  Execute the `check.php` script from the command line:

        php app/check.php

    Or access the `config.php` script from a browser:

        http://localhost/path/to/symfony/app/web/config.php

1. In production mode, run this to compile static assets for production mode:

        php app/console cache:clear
        php app/console assetic:dump --env=prod --no-debug --force
        php app/console assets:install --symlink web

    In development mode, run this:

        php app/console cache:clear
        php app/console assetic:dump --force
        php app/console assets:install --symlink web

1. Create an admin user:

        php app/console fos:user:create --super-admin

1. Setup config files:

        cp app/config/config.yml.dist app/config/config.yml
        cp app/config/parameters.yml.dist app/config/parameters.yml

    Edit to your needs.