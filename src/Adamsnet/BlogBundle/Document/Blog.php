<?php

namespace Adamsnet\BlogBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

// Instructions for creating MongoDB models, and doing CRUD operations on them:
// http://symfony.com/doc/current/bundles/DoctrineMongoDBBundle/index.html

/**
 * @MongoDB\Document
 *
 */
class Blog
{
    /** @MongoDB\Id */
    protected $id;

    /** @MongoDB\String */
    protected $title;

    /** @MongoDB\String */
    protected $author;

    /** @MongoDB\String */
    protected $blog;

    /** @MongoDB\String */
    protected $image;

    /** @MongoDB\String */
    protected $tags;

    protected $comments = array();

    /** @MongoDB\Timestamp */
    protected $created;

    /** @MongoDB\Timestamp */
    protected $updated;

    public function __construct()
    {
        $this->setCreated();
        $this->setUpdated();
    }

    /**
     * @MongoDB\preUpdate
     */
    public function setUpdatedValue()
    {
       $this->setUpdated();
    }


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Blog
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return Blog
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * Get author
     *
     * @return string $author
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set blog
     *
     * @param string $blog
     * @return Blog
     */
    public function setBlog($blog)
    {
        $this->blog = $blog;
        return $this;
    }

    /**
     * Get blog
     *
     * @return string $blog
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Blog
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     *
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set tags
     *
     * @param string $tags
     * @return Blog
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * Get tags
     *
     * @return string $tags
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set created
     *
     * @param timestamp $created
     * @return Blog
     */
    public function setCreated($created = null)
    {
        $this->created = isset($created) ? $created : time();
        return $this;
    }

    /**
     * Get created
     *
     * @return timestamp $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param timestamp $updated
     * @return Blog
     */
    public function setUpdated($updated = null)
    {
        $this->updated = isset($updated) ? $updated : time();
        return $this;
    }

    /**
     * Get updated
     *
     * @return timestamp $updated
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
