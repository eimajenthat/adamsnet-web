<?php
// src/Adamsnet/BlogBundle/Form/BlogType.php

namespace Adamsnet\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class BlogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title');
        $builder->add('author');
        $builder->add('tags', null, array('required' => false));
        $builder->add('blog', 'textarea');
        $builder->add('image', null, array('required' => false));
    }

    public function getName()
    {
        return 'blog';
    }
}