<?php
// src/Adamsnet/BlogBundle/Controller/BlogController.php

namespace Adamsnet\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Adamsnet\BlogBundle\Document\Blog,
    Adamsnet\BlogBundle\Form\BlogType;

/**
 * Blog controller.
 */
class BlogController extends Controller
{
    /**
     * Show a blog entry
     */
    public function showAction($id)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $blog = $dm->getRepository('AdamsnetBlogBundle:Blog')->find($id);
        if (!$blog) {
            throw $this->createNotFoundException('Couldn\'t find blog post ' . $id . '!');
        }

        return $this->render('AdamsnetBlogBundle:Blog:show.html.twig', array(
            'blog'      => $blog,
        ));
    }

    public function listAction()
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $blogs = $dm->getRepository('AdamsnetBlogBundle:Blog')->findAll();
        if (!$blogs) {
            throw $this->createNotFoundException('No blog posts yet.');
        }
        return $this->render('AdamsnetBlogBundle:Blog:list.html.twig', array(
            'blogs'      => $blogs,
        ));
    }

    public function editAction($id)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $blog = $dm->getRepository('AdamsnetBlogBundle:Blog')->find($id);
        if ( false !== is_null($blog) ) {
            throw $this->createNotFoundException('Couldn\'t find blog post ' . $id . '!');
        }

        $form = $this->createForm(new BlogType(), $blog);

        $request = $this->getRequest();
        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $dm->persist($blog);
                $dm->flush();

                // Redirect to blog list
                return $this->redirect($this->generateUrl('AdamsnetBlogBundle_blog_list'));
            }
        }

        return $this->render('AdamsnetBlogBundle:Blog:edit.html.twig', array(
            'form' => $form->createView(),
            'blog' => $blog,
        ));
    }

    public function deleteAction($id)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $blog = $dm->getRepository('AdamsnetBlogBundle:Blog')->find($id);
        if ( false !== is_null($blog) ) {
            throw $this->createNotFoundException('Couldn\'t find blog post ' . $id . '!');
        }

        $dm->remove($blog);
        $dm->flush();

        return $this->redirect($this->generateUrl('AdamsnetBlogBundle_blog_list'));
    }

    public function newAction()
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $blog = new Blog();
        $form = $this->createForm(new BlogType(), $blog);

        $request = $this->getRequest();
        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $dm->persist($blog);
                $dm->flush();

                // Redirect to blog list
                return $this->redirect($this->generateUrl('AdamsnetBlogBundle_blog_list'));
            }
        }

        return $this->render('AdamsnetBlogBundle:Blog:new.html.twig', array(
            'form' => $form->createView()
        ));
    }
}