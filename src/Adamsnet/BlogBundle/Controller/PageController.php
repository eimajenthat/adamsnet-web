<?php
// src/Adamsnet/BlogBundle/Controller/PageController.php

namespace Adamsnet\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Adamsnet\BlogBundle\Document\Enquiry;
use Adamsnet\BlogBundle\Form\EnquiryType;

class PageController extends Controller
{
    public function indexAction()
    {
        return $this->render('AdamsnetBlogBundle:Page:index.html.twig');
    }

    public function aboutAction()
    {
        return $this->render('AdamsnetBlogBundle:Page:about.html.twig');
    }

    public function contactAction()
    {
        $enquiry = new Enquiry();
        $form = $this->createForm(new EnquiryType(), $enquiry);

        $request = $this->getRequest();
        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Contact enquiry from symblog')
                    ->setFrom('web@adamsnet.info')
                    ->setTo($this->container->getParameter('adamsnet_blog.emails.contact_email'))
                    ->setBody($this->renderView('AdamsnetBlogBundle:Page:contactEmail.txt.twig', array('enquiry' => $enquiry)));
                $this->get('mailer')->send($message);

                $this->get('session')->setFlash('blogger-notice', 'Your contact enquiry was successfully sent. Thank you!');



                // Redirect - This is important to prevent users re-posting
                // the form if they refresh the page
                return $this->redirect($this->generateUrl('AdamsnetBlogBundle_contact'));
            }
        }

        return $this->render('AdamsnetBlogBundle:Page:contact.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
